<?php

namespace App\Http\Controllers;

use App\Task;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::all();
        return [
            'message' => 'Fetched all tasks',
            'data' => $tasks
        ];
    }

    public function show($id)
    {
        $task = Task::find($id);
        return [
            'message' => 'Fetch one task',
            'data' => $task
        ];
    }

    public function store($id, $title, $description, $status)
    {
        $task = Task::create([
            'id'=>$id, 
            'title' =>$title, 
            'description' => $description, 
            'status' => $status
        ]);
        return [
            'message' => 'Task succesfully created.',
            'data' => $task
        ];
    }
    public function update($id, $title, $description, $status)
    {
        $task = Task::create([
            'id'=>$id, 
            'title' =>$title, 
            'description' => $description, 
            'status' => $status
        ]);
        return [
            'message' => 'Task succesfully created.',
            'data' => $task
        ];
    }

    public function delete($id)
    {
        Task::where([
            'id' => $id
        ])->delete();
        return [
            'message' => 'Successfully deleted the task'
        ];
    }
}
